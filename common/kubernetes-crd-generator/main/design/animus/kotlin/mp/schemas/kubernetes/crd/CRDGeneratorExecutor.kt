package design.animus.kotlin.mp.schemas.kubernetes.crd

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.JSONSchemaGeneratorExecutor
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.apiextensionsapiserver.pkg.apis.apiextensions.v1.CustomResourceDefinitionSerializer
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.apiextensionsapiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsSerializer
import java.io.File
import kotlinx.serialization.json.Json

suspend fun crdGeneratorExecutor(config: CRDConfig) {
  return config.crds.forEach { crdConfig ->
    println("Generating CRD: ${crdConfig.name}")
    val crd = Json.decodeFromString(CustomResourceDefinitionSerializer, crdConfig.path.readText())

    crd.spec.versions.map { vers ->
      val schemaAsJson = Json.encodeToString(
        JSONSchemaPropsSerializer,
        vers.schema?.openAPIV3Schema?.copy(
          Schema = "http://json-schema.org/draft-07/schema#"
        )
          ?: error("Schema was null")
      )
      val tempFile = File.createTempFile("crd", vers.name)
      tempFile.writeText(schemaAsJson)
      val genConfig = GeneratorConfig(
        packageBase = "${config.packageBase}.${crdConfig.path.nameWithoutExtension}.${vers.name}",
        schemaName = crdConfig.name,
        schemaFile = tempFile,
        createBaseObject = true,
        outputPath = config.outputPath
      )
      File("${config.outputPath}/${genConfig.packageBase.replace(".", "/")}").mkdirs()
      JSONSchemaGeneratorExecutor.generate(tempFile.absolutePath, genConfig)
    }
  }
}
