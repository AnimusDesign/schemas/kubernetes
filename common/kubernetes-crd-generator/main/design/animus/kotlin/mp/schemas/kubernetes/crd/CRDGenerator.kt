package design.animus.kotlin.mp.schemas.kubernetes.crd

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import mu.KotlinLogging
import org.gradle.api.Plugin
import org.gradle.api.Project

internal val logger = KotlinLogging.logger { }

@ExperimentalSerializationApi
class CRDGenerator : Plugin<Project> {
  override fun apply(project: Project) {
    val extension = project.extensions.create<CRDGeneratorConfig>(
      "CRDGeneratorConfig",
      CRDGeneratorConfig::class.java
    )
    project.task("CRDGenerate")
      .doFirst { _ ->
        runBlocking {
          crdGeneratorExecutor(extension?.crdConfig ?: error("CRD Config cannot be null"))
        }
      }
  }
}
