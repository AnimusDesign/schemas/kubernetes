package design.animus.kotlin.mp.schemas.kubernetes.crd

import java.io.File

data class CRD(
  val path: File,
  val name: String
)

data class CRDConfig(
  val packageBase: String,
  val outputPath: File,
  val crds: List<CRD>
)

open class CRDGeneratorConfig(
  var crdConfig: CRDConfig? = null
)
