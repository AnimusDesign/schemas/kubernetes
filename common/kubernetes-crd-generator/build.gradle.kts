import design.animus.kotlin.contract.Versions.Dependencies

plugins {
  id("java-gradle-plugin")
}

val artifactID = "kubernetes-crd-generatorv"
kotlin {
  target {
    mavenPublication {
      artifactId = artifactID
    }
  }
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated")) // , "generated"))
      dependencies {
        api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp.schemas:json_parser:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp.schemas:json_generator:${Dependencies.mpJSONSchema}")
//          implementation("com.benasher44:uuid:${Dependencies.mpUUID}")
//          implementation("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
//          implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
//          implementation("io.ktor:ktor-client-core:${Dependencies.ktor}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
//          implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        api(project(":120:kubernetes-120:"))
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      resources.setSrcDirs(listOf("src/test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("assemble") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
  kotlinOptions.apiVersion = "1.4"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      artifactId = artifactID
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}

gradlePlugin {
  plugins {
    create("crdGenerator") {
      id = "design.animus.kotlin.mp.schemas.kubernetes.crd.generator"
      implementationClass = "design.animus.kotlin.mp.schemas.kubernetes.crd.CRDGenerator"
    }
  }
}
