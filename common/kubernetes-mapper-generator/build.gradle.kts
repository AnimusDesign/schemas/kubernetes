import design.animus.kotlin.contract.Versions.Dependencies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("application")
}
val artifactID = "kubernetes-mapper-generator"
kotlin {
  target {
    mavenPublication {
      artifactId = artifactID
    }
  }
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated")) // , "generated"))
      dependencies {
        implementation(project(":common:kubernetes-common:"))
        api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp.schemas:json_parser:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp.schemas:json_generator:${Dependencies.mpJSONSchema}")
        implementation("design.animus.kotlin.mp.schemas:open_api:${Dependencies.mpOpenAPI}")
        implementation("design.animus.kotlin.mp.schemas:open_api_generator:${Dependencies.mpOpenAPI}")
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        api("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        api("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        api("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      resources.setSrcDirs(listOf("src/test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("build") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("run") {
  dependsOn("build")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      artifactId = artifactID
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}

application {
  mainClassName = "design.animus.kotlin.mp.schemas.kubernetes.mapper.DirectorKt"
}
