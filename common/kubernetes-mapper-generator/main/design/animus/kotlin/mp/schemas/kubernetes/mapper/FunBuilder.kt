package design.animus.kotlin.mp.schemas.kubernetes.mapper

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.mutateNameIfEnabled
import design.animus.kotlin.mp.schemas.json.generator.mutatePackageNameIfEnabled
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.kubernetes.common.UnknownKubernetesRecord
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsSchemas
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

@ExperimentalSerializationApi
suspend fun createKubernetesMapperFun(schemas: Map<String, OneOfComponentsSchemas.PossibleSchema>, config: GeneratorConfig): FunSpec {
  println("In create kubernetes mapper fun")
  println(schemas.size)
  val func = FunSpec.builder("determineSerializerForKubernetesRecord")
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .returns(
      AJSONSchemaSerializer::class.toClassName()
        .parameterizedBy(IJSONSchemaObjectBase::class.toClassName())
    )
    .addParameter("record", UnknownKubernetesRecord::class)
    .beginControlFlow("val serializer = when (record)")
  schemas.forEach { (name, definition) ->
    val apiDef = definition.item.schemaPatternProperties?.map?.get("x-kubernetes-group-version-kind")
      ?: error("Expected a group version kind def but it was null.")
    val recordKind = when (apiDef) {
      is JsonArray -> {
        val firstItem = (apiDef as? JsonArray ?: error("Expected array but found: $apiDef")).get(0)
          as? JsonObject ?: error("Expected json object as first element but found: $apiDef")
        Json.decodeFromJsonElement(
          KubernetesKindVersion.serializer(),
          firstItem
        )
      }
      else -> error("Expected an object definition but found other.")
    }.toUnknownKubernetesRecord()
    val recordName = mutateNameIfEnabled(
      name,
      config
    )
    val packageName = mutatePackageNameIfEnabled(
      "design.animus.kotlin.mp.schemas.kubernetes.generated.definitions",
      name,
      config
    )

    func.addStatement(
      "UnknownKubernetesRecord(apiVersion=%S, kind=%S ) -> %T",
      recordKind.apiVersion,
      recordKind.kind,
      ClassName(packageName, "${recordName.className}Serializer")
    )
  }
  func
    .addStatement(
      "else -> throw %T(\"Could not determine what serializer to use for: \" + record)",
      SerializationException::class
    )
    .endControlFlow()
    .addStatement("return serializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>")
  return func.build()
}
