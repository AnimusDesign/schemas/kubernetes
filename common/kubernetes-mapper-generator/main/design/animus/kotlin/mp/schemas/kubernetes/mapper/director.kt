package design.animus.kotlin.mp.schemas.kubernetes.mapper

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.fileSpecBootStrap
import design.animus.kotlin.mp.schemas.kubernetes.common.UnknownKubernetesRecord
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsSchemas
import java.io.File
import kotlin.js.ExperimentalJsExport
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class KubernetesKindVersion(
  val group: String,
  val version: String,
  val kind: String
) {
  suspend fun toUnknownKubernetesRecord(): UnknownKubernetesRecord {
    return if (this.group.isEmpty()) {
      UnknownKubernetesRecord(this.version, this.kind)
    } else {
      UnknownKubernetesRecord("${this.group}/${this.version}", this.kind)
    }
  }
}

@ExperimentalJsExport
@ExperimentalSerializationApi
fun processSchema(version: Int, schemaPath: File) = runBlocking {
  println("In process schema version: $version, schema: $schemaPath")
  val cwd = System.getProperty("user.dir")
  /**
   * output assumes you run ./gradlew from root of repository
   */
  /**
   * output assumes you run ./gradlew from root of repository
   */
  val config = GeneratorConfig(
    packageBase = "design.animus.kotlin.mp.schemas.kubernetes.mapper.generated",
    outputPath = File("$cwd/../../$version/kubernetes-$version-mapper/generated/commonMain/kotlin"),
    schemaFile = schemaPath,
    schemaName = "Kubernetes",
    createBaseObject = false,
    mutateObjectName = Option.Some { renameContext ->
      // Expected name like Follows
      // io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1.ServiceReference
      // We want just the last piece
      renameContext.itemName.split(".").last()
    },
    mutatePackageName = Option.Some { renameContext ->
      // see package base above for starting point
      // Then we receive a name like above
      val itemNameAsPackageName = renameContext.itemName.split(".")
      // Should end up as "design.animus.kotlin.mp.schemas.kubernetes.generated.io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1"
      "${renameContext.packageName}.${
      itemNameAsPackageName.subList(0, itemNameAsPackageName.size - 1).joinToString(".")
      }"
    }
  )
  val schema = Json {
    ignoreUnknownKeys = true
  }.decodeFromString(OpenAPI.serializer(), config.schemaFile.readText())
  val components = schema.components?.schemas?.schemaPatternProperties?.map ?: error("Components was null")
  val schemas = components.filterValues { value ->
    value is OneOfComponentsSchemas.PossibleSchema &&
      (value as OneOfComponentsSchemas.PossibleSchema).item.schemaPatternProperties != null &&
      (value as OneOfComponentsSchemas.PossibleSchema).item.schemaPatternProperties!!.regex.pattern == """^x-""" &&
      (value as OneOfComponentsSchemas.PossibleSchema).item.schemaPatternProperties!!.map.containsKey("x-kubernetes-group-version-kind")
  } as Map<String, OneOfComponentsSchemas.PossibleSchema>
  val file = fileSpecBootStrap(config.packageBase, "UnknownKubernetesRecordMapper")
  val func = createKubernetesMapperFun(schemas, config)
  file
    .addFunction(func)
    .build()
    .writeTo(config.outputPath)
}

@ExperimentalJsExport
@ExperimentalSerializationApi
suspend fun main() = try {
  println("!!!!!!!!!!!!!")
  println(System.getProperty("user.dir"))
  val cwd = "${System.getProperty("user.dir")}/../.."
  println(cwd)
  println("!!!!!!!!!!!!!")
  val versions = listOf(116, 117, 118, 119, 120)
  versions.forEach { vers ->
    val schemaPath = File("$cwd/$vers/kubernetes-$vers/main/resources/openapi.json")
    processSchema(vers, schemaPath)
  }
} catch (e: Exception) {
  println("----------------")
  println(e.message)
  e.printStackTrace()
}
