package design.animus.kotlin.mp.schemas.kubernetes.mapper

import java.io.File

interface ITestBase {
  fun loadTestSchema(fileName: String): String {
    val clazz = ITestBase::class.java.classLoader.getResource(fileName)
    val file = File(clazz.path)
    return file.readText()
  }
}
