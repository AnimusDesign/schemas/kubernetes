package design.animus.kotlin.mp.schemas.kubernetes.mapper

import design.animus.kotlin.mp.schemas.kubernetes.common.parseToKnownRecord
import design.animus.kotlin.mp.schemas.kubernetes.generated.anonymous.io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelectorMatchLabels
import design.animus.kotlin.mp.schemas.kubernetes.generated.anonymous.io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMetaLabels
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.apps.v1.Deployment
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.apps.v1.DeploymentSerializer
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.apps.v1.DeploymentSpec
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.core.v1.Container
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.core.v1.ContainerPort
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.core.v1.PodSpec
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.core.v1.PodTemplateSpec
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta
import design.animus.kotlin.mp.schemas.kubernetes.mapper.generated.determineSerializerForKubernetesRecord
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlin.test.Test
import kotlin.test.assertTrue
import design.animus.kotlin.mp.schemas.kubernetes.common.parseUnknownKubernetesRecord
import design.animus.kotlin.mp.schemas.kubernetes.common.yamlToJson
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.extensions.v1beta1.Ingress
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.extensions.v1beta1.IngressBackend
import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.extensions.v1beta1.IngressSpec
import kotlinx.serialization.json.Json

@ExperimentalSerializationApi
class SimpleDeployment : ITestBase {
  @Test
  fun testParse() = runBlocking {
    val raw = loadTestSchema("SimpleDeployment.yaml")
    val unknown = parseUnknownKubernetesRecord(raw)
    val serializer = determineSerializerForKubernetesRecord(unknown)
    val deploymentSerializer = serializer as? DeploymentSerializer ?: error("Serializer was not Deployment Serializer as anticipated")
    val deployment = parseToKnownRecord<Deployment>(deploymentSerializer, yamlToJson(raw))
    assertTrue("Deployment is named 'nginx-deployment'") {
      (deployment.metadata?.name != null) &&
        (deployment.metadata!!.name == "nginx-deployment")
    }
    assertTrue("Deployment meta data has labels of: {app: nginx}") {
      (deployment.metadata?.labels != null) &&
        deployment.metadata!!.labels!!.schemaAdditionalProperties.containsKey("app") &&
        deployment.metadata!!.labels!!.schemaAdditionalProperties["app"] == "nginx"
    }
    assertTrue("Spec contains 3 replicas") {
      (deployment.spec != null) &&
        deployment.spec!!.replicas == 3.toShort()
    }
    assertTrue("matchLabels is {app: nginx}") {
      (deployment.spec?.selector?.matchLabels != null) &&
        (deployment.spec as DeploymentSpec).selector.matchLabels?.schemaAdditionalProperties?.containsKey("app") ?: false &&
        (deployment.spec as DeploymentSpec).selector.matchLabels?.schemaAdditionalProperties?.get("app") == "nginx"
    }
    assertTrue("Template spec containers is 1") {
      deployment.spec?.template?.spec?.containers?.size == 1
    }
    val containerSpec = deployment.spec?.template?.spec?.containers?.first()!!
    assertTrue("Container Spec name is nginx") { containerSpec.name == "nginx" }
    assertTrue("Container Spec image is nginx:1.14.2") { containerSpec.image == "nginx:1.14.2" }
    assertTrue("Container exposes port 80") {
      val portSpec = containerSpec.ports?.first()!!
      portSpec.containerPort == 80.toShort()
    }
  }

  suspend fun Deployment.addPodSpec(containers: List<Container>) {
    val podSpec = this.spec?.template?.spec?.copy(containers = containers)
      ?: PodSpec(containers = containers)
  }

  @Test
  fun testSerialize() = runBlocking {
    val commonLabel = mapOf("app" to "nginx")
    val ingress = Ingress(
      spec = IngressSpec(
        backend = IngressBackend(
          serviceName = "http",
          servicePort = 8080
        )
      )
    )
    val deployment = Deployment(
      apiVersion = "apps/v1",
      kind = "Deployment",
      metadata = ObjectMeta(
        name = "nginx-deployment",
        labels = ObjectMetaLabels(schemaAdditionalProperties = commonLabel)
      ),
      spec = DeploymentSpec(
        replicas = 3,
        selector = LabelSelector(
          matchLabels = LabelSelectorMatchLabels(
            schemaAdditionalProperties = commonLabel
          )
        ),
        template = PodTemplateSpec(
          metadata = ObjectMeta(
            labels = ObjectMetaLabels(schemaAdditionalProperties = commonLabel),
          ),
          spec = PodSpec(
            containers = listOf(
              Container(
                image = "nginx:1.14.2",
                name = "nginx",
                ports = listOf(
                  ContainerPort(containerPort = 80)
                )
              )
            )
          )
        )
      )
    )
    val data = Json.encodeToString(DeploymentSerializer, deployment)
    val raw = yamlToJson(loadTestSchema("SimpleDeployment.yaml"))
    assertTrue("Serialized/encoded result matches raw") { raw == data }
  }
}
