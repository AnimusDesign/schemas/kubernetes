package design.animus.kotlin.mp.schemas.kubernetes.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory

actual suspend fun yamlToJson(content: String): String = try {
  val yamlReader = ObjectMapper(YAMLFactory())
  val obj: Any = yamlReader.readValue(content, Any::class.java)
  val jsonWriter = ObjectMapper()
    .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
  jsonWriter.writeValueAsString(obj)
} catch (e: Exception) {
  e.printStackTrace()
  throw e
}
