package design.animus.kotlin.mp.schemas.kubernetes.common

@JsModule("yaml")
@JsNonModule
external val YAML: dynamic

actual suspend fun yamlToJson(content: String): String {
  val data = YAML.parse(content)
  return JSON.stringify(data as? Any)
}
