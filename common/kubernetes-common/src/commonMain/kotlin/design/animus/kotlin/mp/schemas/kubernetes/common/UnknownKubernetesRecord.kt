package design.animus.kotlin.mp.schemas.kubernetes.common

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import mu.KotlinLogging

internal val logger = KotlinLogging.logger {}

@Serializable
data class UnknownKubernetesRecord(val apiVersion: String, val kind: String)

suspend fun parseUnknownKubernetesRecord(raw: String): UnknownKubernetesRecord {
  val json = Json { ignoreUnknownKeys = true }
  return try {
    val asJson = yamlToJson(raw)
    println(asJson)
    json.decodeFromString(UnknownKubernetesRecord.serializer(), asJson)
  } catch (err: Exception) {
    println(err.message)
    json.decodeFromString(UnknownKubernetesRecord.serializer(), raw)
  } catch (e: Exception) {
    logger.error(e) { "Encountered an unexpected exception could not parse as yaml or json." }
    throw e
  }
}

suspend inline fun <reified RSP : IJSONSchemaObjectBase> parseToKnownRecord(
  serializer: AJSONSchemaSerializer<RSP>,
  content: String
): RSP {
  return try {
    val asJson = yamlToJson(content)
    Json.decodeFromString(serializer, content)
  } catch (err: Exception) {
    Json.decodeFromString(serializer, content)
  } catch (e: Exception) {
    throw e
  } as RSP
}
