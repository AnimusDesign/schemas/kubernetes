package design.animus.kotlin.mp.schemas.kubernetes.common

expect suspend fun yamlToJson(content: String): String
