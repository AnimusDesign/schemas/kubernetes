buildscript {
    repositories {
        mavenLocal()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.32")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:10.0.0")
        classpath("com.github.jengelman.gradle.plugins:shadow:6.1.0")
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.4.32")
    }
}

allprojects {
    repositories {
        mavenLocal()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
}

subprojects {
    if (
        project.path.contains(Regex("""kubernetes-\d{3}""")) ||
        project.path.endsWith("kubernetes-common") ||
        project.path.contains("gitlab") ||
        project.path.contains("event-ctrl")
    ) {
        apply(plugin = "org.jetbrains.kotlin.multiplatform")
    }
    if (
        project.path.endsWith("kubernetes-mapper-generator") ||
        project.path.endsWith("kubernetes-crd-generator") ||
            project.path.endsWith("crd")
    ) {
        apply(plugin = "org.jetbrains.kotlin.jvm")
    }
    apply(plugin = "org.jetbrains.kotlin.plugin.serialization")
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
    apply(plugin = "maven-publish")
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
        kotlinOptions.apiVersion = "1.4"
    }
    configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
        debug.set(true)
        verbose.set(true)
        android.set(false)
        enableExperimentalRules.set(true)
        additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
        this.filter(
            Action {
                include("**/kotlin/**", "**/main/**")
                exclude("**/generated/**")
            }
        )
    }
    val version: String by project
    val baseGroup = "design.animus.kotlin.mp.schemas"
    group = if (project.path.contains("extensions")) {
        "${baseGroup}.kubernetes.extensions"
    } else {
        baseGroup
    }
    this.version = version
    configure<PublishingExtension> {
        repositories {
            maven {
                url =
                    java.net.URI("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven/")
                credentials(HttpHeaderCredentials::class.java) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}
