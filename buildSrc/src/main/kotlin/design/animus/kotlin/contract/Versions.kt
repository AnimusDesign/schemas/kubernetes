package design.animus.kotlin.contract

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
        const val gradlePluginPublish = "0.10.1"
    }

    object Dependencies {
        const val kotlin = "1.4.32"
        const val serialization = "1.1.0"
        const val coroutine = "1.4.3"
        const val junit = "4.12"
        const val kotlinPoet = "1.7.2"
        const val kotlinLogging = "2.0.4"
        const val logback = "1.2.3"
        const val MPDataTypes = "0.1.3"
        const val joda = "2.10.5"
        const val mpUUID = "0.2.3"
        const val mpDate = "0.1.1"
        const val mpConfig = "0.1.2"
        const val mpJSONSchema = "0.0.2"
        const val mpOpenAPI = "0.0.1"
        const val kaml = "0.26.0"
        const val jacksonYaml = "2.11.3"
        const val yamlKt = "0.7.5"
        const val ktor = "1.5.2"
    }
}
