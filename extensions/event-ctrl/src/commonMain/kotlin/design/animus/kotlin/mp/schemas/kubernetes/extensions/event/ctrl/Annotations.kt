package design.animus.kotlin.mp.schemas.kubernetes.extensions.event.ctrl

import co.remotectrl.ctrl.event.CtrlAggregate

suspend fun <A : co.remotectrl.ctrl.event.CtrlAggregate<A>> CtrlAggregate<A>.createAnnotations(): Map<String, Any> {
  return mapOf(
    "event-ctrl/id" to this.legend.aggregateId,
    "event-ctrl/version" to this.legend.latestVersion
  )
}
