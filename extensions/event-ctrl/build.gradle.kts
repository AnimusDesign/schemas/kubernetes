import design.animus.kotlin.contract.Versions.Dependencies

kotlin {
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "11"
    }
  }
  js {
    nodejs()
    binaries.executable()
  }
  sourceSets {
    val commonMain by getting {
      dependencies {
        api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp:config:${Dependencies.mpConfig}")
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("io.github.microutils:kotlin-logging:${Dependencies.kotlinLogging}")
        implementation("ctrl.remote:event-ctrl:${Dependencies.eventCtrl}")
        implementation(project(":120:kubernetes-120:"))
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      dependencies {
      }
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test-junit"))
      }
    }
    val jsMain by getting {
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
  }
}
