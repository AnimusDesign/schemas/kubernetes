package design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab

import design.animus.kotlin.mp.configuration.env.Environment
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeCaPemFile
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeConfig
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeIngressBaseDomain
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeNamespace
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeToken
import design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab.GitLabKubeConfig.kubeUrl

/**
 * GitLab CI/CD Variables for Kubernetes
 *
 * @since 0.0.1
 * @property kubeUrl Equal to the API URL.
 * @property kubeToken The Kubernetes token of the environment service account. Prior to GitLab 11.5, KUBE_TOKEN was the Kubernetes token of the main service account of the cluster integration.
 * @property kubeNamespace The namespace associated with the project’s deployment service account. In the format <project_name>-<project_id>-<environment>. For GitLab-managed clusters, a matching namespace is automatically created by GitLab in the cluster. If your cluster was created before GitLab 12.2, the default KUBE_NAMESPACE is set to <project_name>-<project_id>.
 * @property kubeCaPemFile    Path to a file containing PEM data. Only present if a custom CA bundle was specified.
 * @property kubeConfig Path to a file containing kubeconfig for this deployment. CA bundle would be embedded if specified. This configuration also embeds the same token defined in KUBE_TOKEN so you likely need only this variable. This variable name is also automatically picked up by kubectl so you don’t need to reference it explicitly if using kubectl.
 * @property kubeIngressBaseDomain From GitLab 11.8, this variable can be used to set a domain per cluster. See cluster domains for more information.
 */
object GitLabKubeConfig {
  val kubeUrl: String by Environment("KUBE_URL")
  val kubeToken: String by Environment("KUBE_TOKEN")
  val kubeNamespace: String by Environment("KUBE_NAMESPACE")
  val kubeCaPemFile: String by Environment("KUBE_CA_PEM_FILE")
  val kubeConfig: String by Environment("KUBECONFIG")
  val kubeIngressBaseDomain: String by Environment("KUBE_INGRESS_BASE_DOMAIN")
}
