package design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab

import design.animus.kotlin.mp.schemas.kubernetes.generated.definitions.io.k8s.api.core.v1.Container
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
/**
 * Assumes the following '$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$name:$CI_COMMIT_SHA'
 */
suspend fun createContainerFromGitLab(name: String): Container {
  return Container(
    name = name,
    image = "${GitLabEnvConfig.ciRegistryImage}/${GitLabEnvConfig.ciCommitRefSlug}/$name:${GitLabEnvConfig.ciCommitSha}"
  )
}
