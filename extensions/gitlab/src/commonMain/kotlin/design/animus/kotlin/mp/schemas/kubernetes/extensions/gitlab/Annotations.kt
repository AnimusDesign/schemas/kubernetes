package design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab

val gitLabDeploymentAnnotations = mutableMapOf<String, String>(
  "app.gitlab.com/app" to GitLabEnvConfig.ciProjectPathSlug,
  "app.gitlab.com/env" to GitLabEnvConfig.ciEnvironmentSlug,
)

val gitLabRepoAnnotations = mutableMapOf(
  "gitlab/projectName" to GitLabEnvConfig.ciProjectName,
  "gitlab/commitSha" to GitLabEnvConfig.ciCommitSha,
  "gitlab/projectUrl" to GitLabEnvConfig.ciProjectUrl
)
