package design.animus.kotlin.mp.schemas.kubernetes.extensions.gitlab

import design.animus.kotlin.mp.configuration.env.Environment

/**
 * An object exposing environment variables from a gitlab pipeline.
 *
 * @since 0.0.1
 */
object GitLabEnvConfig {
  val ciProjectPathSlug: String by Environment("CI_PROJECT_PATH_SLUG")
  val ciEnvironmentSlug: String by Environment("CI_ENVIRONMENT_SLUG")
  val ciJobId: String by Environment("CI_JOB_ID")
  val ciCommitSha: String by Environment("CI_COMMIT_SHA")
  val ciCommitRefSlug: String by Environment("CI_COMMIT_REF_SLUG")
  val ciCommitShortSha: String by Environment("CI_COMMIT_SHORT_SHA")
  val ciCommitRefName: String by Environment("CI_COMMIT_REF_NAME")
  val ciRepositoryUrl: String by Environment("CI_REPOSITORY_URL")
  val ciCommitTag: String by Environment("CI_COMMIT_TAG")
  val ciJobName: String by Environment("CI_JOB_NAME")
  val ciJobStage: String by Environment("CI_JOB_STAGE")
  val ciJobManual: String by Environment("CI_JOB_MANUAL")
  val ciJobTriggered: String by Environment("CI_JOB_TRIGGERED")
  val ciJobToken: String by Environment("CI_JOB_TOKEN")
  val ciPipelineId: String by Environment("CI_PIPELINE_ID")
  val ciPipelineIId: String by Environment("CI_PIPELINE_IID")
  val ciPagesDomain: String by Environment("CI_PAGES_DOMAIN")
  val ciPagesURL: String by Environment("CI_PAGES_URL")
  val ciProjectId: String by Environment("CI_PROJECT_ID")
  val ciProjectDir: String by Environment("CI_PROJECT_DIR")
  val ciProjectName: String by Environment("CI_PROJECT_NAME")
  val ciProjectTitle: String by Environment("CI_PROJECT_TITLE")
  val ciProjectNameSpace: String by Environment("CI_PROJECT_NAMESPACE")
  val ciProjectRootNameSpace: String by Environment("CI_PROJECT_ROOT_NAMESPACE")
  val ciProjectPath: String by Environment("CI_PROJECT_PATH")
  val ciProjectUrl: String by Environment("CI_PROJECT_URL")
  val ciRegistry: String by Environment("CI_REGISTRY")
  val ciRegistryImage: String by Environment("CI_REGISTRY_IMAGE")
  val ciRegistryUser: String by Environment("CI_REGISTRY_USER")
  val ciRegistryPassword: String by Environment("CI_REGISTRY_PASSWORD")
  val ciRunnerId: String by Environment("CI_RUNNER_ID")
  val ciRunnerDescription: String by Environment("CI_RUNNER_DESCRIPTION")
  val ciRunnerTags: String by Environment("CI_RUNNER_TAGS")
  val ciServers: String by Environment("CI_SERVER")
  val ciServerUrl: String by Environment("CI_SERVER_URL")
  val ciServerHost: String by Environment("CI_SERVER_HOST")
  val ciServerPort: String by Environment("CI_SERVER_PORT")
  val ciServerProtocol: String by Environment("CI_SERVER_PROTOCOL")
  val ciServerName: String by Environment("CI_SERVER_NAME")
  val ciServerRevision: String by Environment("CI_SERVER_REVISION")
  val ciServerVersion: String by Environment("CI_SERVER_VERSION")
  val ciServerRevisionMajor: String by Environment("CI_SERVER_VERSION_MAJOR")
  val ciServerVersionMinor: String by Environment("CI_SERVER_VERSION_MINOR")
  val ciServerVersionPatch: String by Environment("CI_SERVER_VERSION_PATCH")
  val gitlabUserEmail: String by Environment("GITLAB_USER_EMAIL")
  val gitlabUserId: String by Environment("GITLAB_USER_ID")
}
