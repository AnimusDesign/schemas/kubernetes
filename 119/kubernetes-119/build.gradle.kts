import design.animus.functional.datatypes.option.Option
import design.animus.functional.datatypes.option.Option.Some
import design.animus.kotlin.contract.Versions.Dependencies
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.OpenAPIConfig
import java.io.File
plugins {
  id("design.animus.kotlin.mp.schemas.open_api.generator")
  kotlin("multiplatform")
}

kotlin {
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "11"
      kotlinOptions.apiVersion = "1.4"
    }
  }
  js(BOTH) {
    browser()
    nodejs()
    compilations.named("main") {
      kotlinOptions {
        metaInfo = true
        sourceMap = true
        verbose = true
        moduleKind = "umd"
      }
    }
  }
  val hostOs = System.getProperty("os.name")
  val isMingwX64 = hostOs.startsWith("Windows")
  val nativeTarget = when {
    hostOs == "Mac OS X" -> macosX64("macOS")
    hostOs == "Linux" -> linuxX64("linuxX64")
    isMingwX64 -> mingwX64("mingwX64")
    else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
  }

  sourceSets {
    val commonMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/commonMain/kotlin", "generated/commonMain/kotlin"))
      dependencies {
        api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("io.github.microutils:kotlin-logging:${Dependencies.kotlinLogging}")
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/jvmMain/kotlin", "generated/jvmMain/kotlin"))
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test-junit"))
      }
    }
    val jsMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/jsMain/kotlin", "generated/jsMain/kotlin"))
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
    val nativeMain by creating {
      dependsOn(commonMain)
    }
    val nativeTest by creating {
      dependsOn(commonMain)
    }
    when (nativeTarget.name) {
      "macOS" -> {
        val macOSMain by getting {
          dependsOn(nativeMain)
        }
        val macOSTest by getting {
          dependsOn(nativeTest)
        }
      }
      "mingwX64" -> {
        val mingwX64Main by getting {
          dependsOn(nativeMain)
        }
        val mingwX64Test by getting {
          dependsOn(nativeTest)
        }
      }
      "linuxX64" -> {
        val linuxX64Main by getting {
          dependsOn(nativeMain)
        }
        val linuxX64Test by getting {
          dependsOn(nativeTest)
        }
      }
    }
  }
}

OpenAPIGeneratorConfig {
  schemas = listOf(
    OpenAPIConfig(
      GeneratorConfig(
        packageBase = "design.animus.kotlin.mp.schemas.kubernetes.generated",
        outputPath = File("$projectDir/generated/commonMain/kotlin"),
        schemaFile = File("$projectDir/main/resources/openapi.json"),
        schemaName = "Kubernetes",
        createBaseObject = false,
        mutateObjectName = Option.Some { renameContext ->
          // Expected name like Follows
          // io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1.ServiceReference
          // We want just the last piece
          renameContext.itemName.split(".").last()
        },
        mutatePackageName = Option.Some { renameContext ->
          // see package base above for starting point
          // Then we receive a name like above
          val itemNameAsPackageName = renameContext.itemName.split(".")
          // Should end up as "design.animus.kotlin.mp.schemas.kubernetes.generated.io.k8s.kube-aggregator.pkg.apis.apiregistration.v1beta1"
          "${renameContext.packageName}.${
          itemNameAsPackageName.subList(0, itemNameAsPackageName.size - 1).joinToString(".")
          }"
        }
      )
    )
  )
}
