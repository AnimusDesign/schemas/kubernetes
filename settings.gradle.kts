enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "kubernetes"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.20")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.32")
            }
            if (
                requested.id.id.startsWith("design.animus.kotlin.mp.schemas.json.generator")
            ) {
                useVersion("0.0.2")
            }
            if (
                requested.id.id.startsWith("design.animus.kotlin.mp.schemas.open_api.generator")
            ) {
                useVersion("0.0.1-SNAPSHOT")
            }
            if (
                requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
            ) {
                useVersion("2.0.0")
            }
            if (requested.id.id.startsWith("net.researchgate.release")) {
                // Update Version Build Source if being changed.
                useVersion("2.8.1")
            }

        }
    }
    repositories {
        mavenLocal()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        mavenCentral()
        jcenter()
        gradlePluginPortal()
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
        maven { url = java.net.URI("https://kotlin.bintray.com/kotlin-dev") }
    }
}

include(":common:kubernetes-common:")
include(":common:kubernetes-mapper-generator:")
include(":common:kubernetes-crd-generator:")

include(":116:kubernetes-116:")
include(":116:kubernetes-116-mapper:")

include(":117:kubernetes-117:")
include(":117:kubernetes-117-mapper:")

include(":118:kubernetes-118:")
include(":118:kubernetes-118-mapper:")

include(":119:kubernetes-119:")
include(":119:kubernetes-119-mapper:")

include(":120:kubernetes-120:")
include(":120:kubernetes-120-mapper:")

include(":extensions:gitlab")
