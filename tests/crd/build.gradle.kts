import design.animus.kotlin.contract.Versions.Dependencies
import design.animus.kotlin.mp.schemas.kubernetes.crd.CRDGeneratorConfig
import design.animus.kotlin.mp.schemas.kubernetes.crd.CRD
plugins {
    id ("design.animus.kotlin.mp.schemas.kubernetes.crd.generator") version "0.0.1-SNAPSHOT"
}

kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(mutableListOf("main", "generated")) // , "generated"))
            dependencies {
                api(project(":120:kubernetes-120:"))
                api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
                api("design.animus.kotlin.mp.schemas:json_parser:${Dependencies.mpJSONSchema}")
                api("design.animus.kotlin.mp.schemas:json_generator:${Dependencies.mpJSONSchema}")
                api("design.animus.kotlin.mp.schemas:open_api_generator:${Dependencies.mpOpenAPI}")
                api("design.animus.kotlin.mp.schemas:open_api:${Dependencies.mpOpenAPI}")
                api("com.benasher44:uuid:${Dependencies.mpUUID}")
                api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
                api("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
                api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
                api("io.ktor:ktor-client-core:${Dependencies.ktor}")
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
                api("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                api("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
                implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
            }
        }
        test {
            resources.setSrcDirs(listOf("src/test/resources"))
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
            }
        }
    }
}

task("copyTestResources", Copy::class) {
    from("$projectDir/test/resources")
    into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
    from("$projectDir/main/resources")
    into("$buildDir/classes/kotlin/main")
}
tasks.named("assemble") {
    dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
    dependsOn("processTestResources", "copyTestResources")
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}

CRDGeneratorConfig {
    crdConfig = design.animus.kotlin.mp.schemas.kubernetes.crd.CRDConfig(
        packageBase = "design.animus.kotlin.mp.schemas.kubernetes.test.crd",
        outputPath = File("$projectDir/generated/"),
        crds = listOf(
            CRD(
                path=File("$projectDir/main/resources/podmonitors.monitoring.coreos.com.json"),
                name="PodMonitor"
            )
        )
    )
}
