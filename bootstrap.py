#!/usr/bin/env python3
import urllib.request
import os
import subprocess

versions = {
  120: "v1.20.5",
  119: "v1.19.9",
  118: "v1.18.17",
  117: "v1.17.17",
  116: "v1.16.15"
}

for key, vers in versions.items():
  print(key, vers)
  url = f"https://raw.githubusercontent.com/kubernetes/kubernetes/{vers}/api/openapi-spec/swagger.json"
  response = urllib.request.urlopen(url)
  data = response.read()
  text = data.decode('utf-8')
  print(f"Downloading from url: {url}")
  path = f"{key}/kubernetes-{key}/main/resources"
  print(path)
  print("-" * 30)
  if not os.path.exists(path):
    os.makedirs(path)
  with open(f"{path}/swagger.json", "w") as spec:
    spec.write(text)
  list_files = subprocess.run([
    "java",
    "-jar", "openapi-generator-cli.jar", "generate", "-i", f"./{key}/kubernetes-{key}/main/resources/swagger.json", "-g",
    "openapi", "-o", f"./{key}/kubernetes-{key}/main/resources/"
  ])
