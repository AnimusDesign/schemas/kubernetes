import design.animus.kotlin.contract.Versions.Dependencies

kotlin {
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "11"
      kotlinOptions.apiVersion = "1.4"
    }
  }
  js {
    browser {}
    nodejs {}
    binaries.executable()
  }
//  val hostOs = System.getProperty("os.name")
//  val isMingwX64 = hostOs.startsWith("Windows")
//  val nativeTarget = when {
//    hostOs == "Mac OS X" -> macosX64("native")
//    hostOs == "Linux" -> linuxX64("native")
//    isMingwX64 -> mingwX64("native")
//    else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
//  }
  sourceSets {
    val commonMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/commonMain/kotlin", "generated/commonMain/kotlin"))
      dependencies {
        api(project(":117:kubernetes-117:"))
        api(project(":common:kubernetes-common:"))
        api("design.animus.kotlin.mp.schemas:json_core:${Dependencies.mpJSONSchema}")
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("io.github.microutils:kotlin-logging:${Dependencies.kotlinLogging}")
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/jvmMain/kotlin", "generated/jvmMain/kotlin"))
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test-junit"))
      }
    }
    val jsMain by getting {
      kotlin.setSrcDirs(mutableListOf("src/jsMain/kotlin", "generated/jsMain/kotlin"))
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
//    val nativeMain by getting {
//      kotlin.setSrcDirs(mutableListOf("src/nativeMain/kotlin", "generated/nativeMain/kotlin"))
//    }
//    val nativeTest by getting
  }
}
